#include "jclient.h"
#include "colors.h"
#include "stdafx.h"
#include "targetver.h"
#include <windows.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define N 10

typedef struct koord Koord;

struct koord {
	int x;
	int y;
	int isMine = 0;
};

//Minen zufällig erzeugen
koord erzeugeFeld() {
		koord mine;

		mine.x = rand() % N;
		mine.y = rand() % N;

		//Minen sichtbar machen
		//farbe2(mine.x, mine.y, BLACK);

		return(mine);

}
int main() {

	int anzMinen = 30;

	koord feld[N][N];
	koord minen[49];

	int zahl;
	koord click;

	koord win[N][N];
	int winCounter = 0;
	int clickCounter = 0;

	int counter = 48;

	srand(time(NULL));

	printf("MINESWEEPER\n______________\n");


	flaeche(WHITE);
	formen("c");

	for (int i = 0; i <= anzMinen; i++) {

		minen[i] = erzeugeFeld();

		feld[minen[i].x][minen[i].y].isMine = 1;

		win[minen[i].x][minen[i].y].isMine = 1;

	}


	for (;;) {

		char *a = abfragen();

		if (a[0] == '#') {

			sscanf_s(a, "# %d %d %d", &zahl, &click.x, &click.y);

			printf("x = %d; y = %d\n", click.x, click.y);

			clickCounter++;

			printf("Klicks: %d\n______________\n", clickCounter);

			//falls eine Mine angeklickt wurde -> Niederlage(Animation)
			if (feld[click.x][click.y].isMine == 1) {

				farbe2(click.x, click.y, YELLOW);
				form2(click.x, click.y, "*");
				symbolGroesse2(click.x, click.y, 0.4);

				Sleep(500);

				farbe2(click.x, click.y, ORANGE);
				form2(click.x, click.y, "*");
				symbolGroesse2(click.x, click.y, 0.7);

				Sleep(500);

				farbe2(click.x, click.y, RED);
				form2(click.x, click.y, "*");
				symbolGroesse2(click.x, click.y, 1.0);

				Sleep(500);

				farbe2(click.x, click.y, BLACK);
				form2(click.x, click.y, "c");
				symbolGroesse2(click.x, click.y, 0.5);

				Sleep(400);

				for (int i = 52; i <= 56; i++) {
					form(i, "none");
				}

				text2(2, 5, "B");
				text2(3, 5, "O");
				text2(4, 5, "O");
				text2(5, 5, "M");
				text2(6, 5, "!");

				Sleep(800);

				exit(0);
				return(0);
			}
			//Überprüfung anliegender Mienen
			else {
				farbe2(click.x, click.y, CORNSILK);

				counter = 48;

				if (click.x == 0) {
					//linke untere Ecke
					if (click.y == 0) {
						if (feld[click.x + 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y + 1].isMine == 1) { counter++; }
					}
					//linke Seite
					else {
						if (feld[click.x + 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y - 1].isMine == 1) { counter++; }
					}
				}

				else if (click.y == N - 1) {
					//linke obere Ecke
					if (click.x == 0) {
						if (feld[click.x + 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y - 1].isMine == 1) { counter++; }
					}
					//obere Seite
					else {
						if (feld[click.x + 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y - 1].isMine == 1) { counter++; }
					}
				}

				else if (click.x == N - 1) {
					//rechte obere Ecke
					if (click.y == N - 1) {
						if (feld[click.x - 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y - 1].isMine == 1) { counter++; }
					}
					//rechte Seite
					else {
						if (feld[click.x - 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y - 1].isMine == 1) { counter++; }
						if (feld[click.x][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y + 1].isMine == 1) { counter++; }
					}
				}
				else if (click.y == 0) {
					//rechte untere Ecke 
					if (click.x == N - 1) {
						if (feld[click.x][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y + 1].isMine == 1) { counter++; }
					}
					//untere Seite
					else {
						if (feld[click.x][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x - 1][click.y + 1].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y].isMine == 1) { counter++; }
						if (feld[click.x + 1][click.y + 1].isMine == 1) { counter++; }

					}
				}
				//restliches Feld
				else {
					if (feld[click.x + 1][click.y].isMine == 1) { counter++; }
					if (feld[click.x][click.y + 1].isMine == 1) { counter++; }
					if (feld[click.x - 1][click.y].isMine == 1) { counter++; }
					if (feld[click.x][click.y - 1].isMine == 1) { counter++; }
					if (feld[click.x + 1][click.y + 1].isMine == 1) { counter++; }
					if (feld[click.x - 1][click.y - 1].isMine == 1) { counter++; }
					if (feld[click.x + 1][click.y - 1].isMine == 1) { counter++; }
					if (feld[click.x - 1][click.y + 1].isMine == 1) { counter++; }
				}
				//Angeklickte stellen auf 1 setzen
				win[click.x][click.y].isMine = 1;

				//an Minen anliegende Felder beschreiben
				char c = (char)counter;

				zeichen2(click.x, click.y, c);
				
				//Angeklickte stellen zählen
				for (int j = 0; j <= N - 1; j++) {
					for (int k = 0; k <= N - 1; k++) {
						if (win[j][k].isMine == 1) {
							winCounter++;
						}
					}
				}
				//falls alle minenfreien Felder angeklickt wurden -> Sieg
				if (winCounter == 100) {

					for (int i = 51; i <= 58; i++) {
						form(i, "none");
					}

					text2(1, 5, "G");
					text2(2, 5, "E");
					text2(3, 5, "W");
					text2(4, 5, "O");
					text2(5, 5, "N");
					text2(6, 5, "N");
					text2(7, 5, "E");
					text2(8, 5, "N");

					exit(0);
					return(0);
				}
				else {
					winCounter = 0;
				}

				
			}

		}

	}
}
			

