MINESWEEPER
==================================================================================================================================================

Das Projekt ist ein Spiel mit folgendem Inhalt.

Ein Feld mit einer gewissen Zahl an zufällig erscheinenden Minen wird generiert.  
Dieses Feld bleibt jedoch für den Spieler verdeckt.  
Er kann nicht erkennen, ob ein Feld leer, oder mit einer Mine versehen ist.  
Die Aufgabe des Spielers ist es, das ganze Spielfeld freizulegen, 
ohne dass eine Mine freigelegt wird.  
In den Feldern, die an eine Mine angrenzen, werden Zahlen angezeigt,  
die beschreiben, wieviele Minen an diesem Feld benachbart liegen.  
Das Spiel wird beendet, sobald ein Feld mit einer Mine angeklickt wird.  

Am schwierigsten an der Realisierung dieses Projekts wird vermutlich der Umgang mit abgedeckten Bereichen sein,  
das heißt festzulegen, 
welche Bereiche wann abgedeckt sind und welche sich bei Mausklick aufdecken.